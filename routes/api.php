<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\DictionaryController;
use App\Http\Controllers\Api\DictionaryItemController;
use App\Http\Controllers\Api\LinkController;
use App\Http\Controllers\Api\PasswordController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\VacancyDefaultFieldsController;
use App\Http\Controllers\Api\VacancyTemplateController;
use App\Http\Controllers\Api\VacancyTemplateFieldController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PageController;
use App\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group( ["prefix" => "auth"], function() {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('generateResetCode', [PasswordController::class, 'generateResetCode']);
    Route::post('passwordReset', [PasswordController::class, 'reset']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
});

Route::group( ['middleware' => 'auth:sanctum'], function() {
    Route::group( ["prefix" => "dictionaries"], function() {
        Route::get('departments', [DictionaryController::class, 'departments']);
        Route::get('linkTargetTypes', [DictionaryController::class, 'linkTargetTypes']);
        Route::get('vacancyFieldTypes', [DictionaryController::class, 'vacancyFieldTypes']);
        Route::get('dictionaries', [DictionaryController::class, 'dictionaries']);
    });

    Route::group( ["prefix" => "settings"], function() {
        Route::group(["prefix" => "users"], function () {
            Route::get('index', [UserController::class, 'index']);
            Route::get('{user}', [UserController::class, 'show']);
            Route::post('store', [UserController::class, 'store']);
            Route::post('{user}', [UserController::class, 'update']);
            Route::delete('{user}', [UserController::class, 'destroy']);
        });

        Route::group(["prefix" => "departments"], function () {
            Route::get('index', [DepartmentController::class, 'index']);
            Route::get('{department}', [DepartmentController::class, 'show']);
            Route::post('store', [DepartmentController::class, 'store']);
            Route::post('{department}', [DepartmentController::class, 'update']);
            Route::delete('{department}', [DepartmentController::class, 'destroy']);

            Route::get('{department}/users', [DepartmentController::class, 'users']);
        });

        Route::group(["prefix" => "links"], function () {
            Route::get('index', [LinkController::class, 'index']);
            Route::get('{link}', [LinkController::class, 'show']);
            Route::post('store', [LinkController::class, 'store']);
            Route::post('{link}', [LinkController::class, 'update']);
            Route::delete('{link}', [LinkController::class, 'destroy']);
        });

        Route::group(["prefix" => "pages"], function () {
            Route::get('index', [PageController::class, 'index']);
            Route::get('{page}', [PageController::class, 'show']);
            Route::post('store', [PageController::class, 'store']);
            Route::post('{page}', [PageController::class, 'update']);
            Route::delete('{page}', [PageController::class, 'destroy']);
        });

        Route::group(["prefix" => "dictionaries"], function () {
            Route::get('index', [DictionaryController::class, 'index']);
            Route::get('{dictionary}', [DictionaryController::class, 'show']);
            Route::post('store', [DictionaryController::class, 'store']);
            Route::post('{dictionary}', [DictionaryController::class, 'update']);
            Route::delete('{dictionary}', [DictionaryController::class, 'destroy']);
        });

        Route::group(["prefix" => "dictionaries/{dictionary}/items"], function () {
            Route::get('index', [DictionaryItemController::class, 'index']);
            Route::get('{item}', [DictionaryItemController::class, 'show']);
            Route::post('store', [DictionaryItemController::class, 'store']);
            Route::post('{item}', [DictionaryItemController::class, 'update']);
            Route::delete('{item}', [DictionaryItemController::class, 'destroy']);
        });

        Route::group(["prefix" => "vacancyTemplates"], function () {
            Route::get('index', [VacancyTemplateController::class, 'index']);
            Route::get('{vacancyTemplate}', [VacancyTemplateController::class, 'show']);
            Route::post('store', [VacancyTemplateController::class, 'store']);
            Route::post('{vacancyTemplate}', [VacancyTemplateController::class, 'update']);
            Route::delete('{vacancyTemplate}', [VacancyTemplateController::class, 'destroy']);
        });

        Route::group(["prefix" => "vacancyTemplates/{vacancyTemplate}/fields"], function () {
            Route::get('index', [VacancyTemplateFieldController::class, 'index']);
            Route::get('{field}', [VacancyTemplateFieldController::class, 'show']);
            Route::post('store', [VacancyTemplateFieldController::class, 'store']);
            Route::post('{field}', [VacancyTemplateFieldController::class, 'update']);
            Route::delete('{field}', [VacancyTemplateFieldController::class, 'destroy']);
        });

        Route::group(["prefix" => "profile"], function () {
            Route::get('view', [ProfileController::class, 'view']);
            Route::post('updateData', [ProfileController::class, 'updateData']);
            Route::post('updatePassword', [ProfileController::class, 'updatePassword']);
        });
    });
});

Route::get('/test', function () {
    return "test ok!";
});
