<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDictionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_en');
            $table->string('name_kk');
            $table->string('name_ru');
            $table->timestamps();
        });

        Schema::create('dictionary_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('dictionary_id');
            $table->string('name_en');
            $table->string('name_kk');
            $table->string('name_ru');

            $table->foreign('dictionary_id')
                ->references('id')
                ->on('dictionaries')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionaries');
    }
}
