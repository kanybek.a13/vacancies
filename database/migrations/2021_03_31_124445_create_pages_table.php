<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alias')->unique();
            $table->string('name_en')->default(null)->nullable();
            $table->string('name_kk')->default(null)->nullable();
            $table->string('name_ru')->default(null)->nullable();
            $table->boolean('is_published')->default(false);
            $table->longText('description_en')->default(null)->nullable();
            $table->longText('description_kk')->default(null)->nullable();
            $table->longText('description_ru')->default(null)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
