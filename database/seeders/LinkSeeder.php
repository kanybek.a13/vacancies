<?php

namespace Database\Seeders;

use App\Models\Link;
use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Link::create([
            'name_en' => 'About us',
            'name_kk' => 'Біз туралы',
            'name_ru' => 'О нас',
            'link_en' => '',
            'link_kk' => '',
            'link_ru' => '',
            'target' => '_self',
            'sort' => '1',
        ]);

        Link::create([
            'name_en' => 'Vacancies',
            'name_kk' => 'Бос орындар',
            'name_ru' => 'Вакансии',
            'link_en' => '',
            'link_kk' => '',
            'link_ru' => '',
            'target' => '_self',
            'sort' => '2',
        ]);

        Link::create([
            'name_en' => 'Contacts',
            'name_kk' => 'Байланыстар',
            'name_ru' => 'Контакты',
            'link_en' => '',
            'link_kk' => '',
            'link_ru' => '',
            'target' => '_self',
            'sort' => '3',
        ]);
    }
}
