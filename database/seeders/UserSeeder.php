<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
//use Spatie\Permission\Models\Permission;
//use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'store_update']);
        Permission::create(['name' => 'view']);
        Permission::create(['name' => 'delete']);

        $admin_role = Role::create(['name' => 'admin']);
        $manager_role = Role::create(['name' => 'manager']);
        $employee_role = Role::create(['name' => 'employee']);

        $admin_role->givePermissionTo('store_update');
        $admin_role->givePermissionTo('view');
        $admin_role->givePermissionTo('delete');

        $manager_role->givePermissionTo('store_update');
        $manager_role->givePermissionTo('view');

        $employee_role->givePermissionTo('view');

        $admin = User::create([
            'name' => 'Admin Fullname',
            'email' => 'admin@gmail.com',
            'iin' => 111111111113,
            'email_verified_at' => now(),
            'password' => Hash::make('admin'),
        ]);

        $manager = User::create([
            'name' => 'Manager Fullname',
            'email' => 'kanybek.a13@gmail.com',
            'iin' => 111111111111,
            'email_verified_at' => now(),
            'password' => Hash::make('manager'),
        ]);

        $employee = User::create([
            'name' => 'Employee Fullname',
            'email' => 'kanybek.a13@mail.ru',
            'iin' => 111111111112,
            'email_verified_at' => now(),
            'password' => Hash::make('employee'),
        ]);

        $admin->assignRole($admin_role);
        $manager->assignRole($manager_role);
        $employee->assignRole($employee_role);
    }
}
