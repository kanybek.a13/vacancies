<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'name_en' => 'HR',
            'name_kk' => 'Кадрлар бөлімі',
            'name_ru' => 'Отдел кадров',
        ]);

        Department::create([
            'name_en' => 'PR',
            'name_kk' => 'Қоғаммен байланыс',
            'name_ru' => 'Связям с общественностью',
        ]);

        Department::create([
            'name_en' => 'Administration',
            'name_kk' => 'Администрация',
            'name_ru' => 'Администрация',
        ]);

        Department::create([
            'name_en' => 'Marketing',
            'name_kk' => 'Администрация',
            'name_ru' => 'Администрация',
        ]);
    }
}
