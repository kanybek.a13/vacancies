<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        Page::create([
            'name_en' => 'Home',
            'name_kk' => 'Негізгі',
            'name_ru' => 'Главная',
            'description_en' => json_encode([
                'company_name'=> $faker->company,
                'slogan'=> $faker->sentence,
                'text'=> $faker->text,
                'background'=> $faker->imageUrl()
            ]),
            'description_kk' => json_encode([
                'company_name'=> $faker->company,
                'slogan'=> $faker->sentence,
                'text'=> $faker->text,
                'background'=> $faker->imageUrl()
            ]),
            'description_ru' => json_encode([
                'company_name'=> $faker->company,
                'slogan'=> $faker->sentence,
                'text'=> $faker->text,
                'background'=> $faker->imageUrl()
            ]),
            'alias' => 'home',
        ]);

        foreach (range(0, 3) as $i) {
            Page::create([
                'name_en' => "Page " . $i,
                'name_kk' => "Бет " . $i,
                'name_ru' => "Страница " . $i,
                'description_en' => json_encode([
                    'company_name'=> $faker->company,
                    'slogan'=> $faker->sentence,
                    'text'=> $faker->text,
                    'background'=> $faker->imageUrl()
                ]),
                'description_kk' => json_encode([
                    'company_name'=> $faker->company,
                    'slogan'=> $faker->sentence,
                    'text'=> $faker->text,
                    'background'=> $faker->imageUrl()
                ]),
                'description_ru' => json_encode([
                    'company_name'=> $faker->company,
                    'slogan'=> $faker->sentence,
                    'text'=> $faker->text,
                    'background'=> $faker->imageUrl()
                ]),
                'alias' => Str::slug("Page" . $i)
            ]);
        }

    }
}
