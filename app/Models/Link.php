<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Link
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_kk
 * @property string $name_ru
 * @property string $target
 * @property string $link_en
 * @property string $link_kk
 * @property string $link_ru
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Link newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Link newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Link query()
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereLinkEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereLinkKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereLinkRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereNameKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Link extends Model
{
    use HasFactory;

    protected $guarded = [];
}
