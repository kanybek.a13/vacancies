<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property string $alias
 * @property string|null $name_en
 * @property string|null $name_kk
 * @property string|null $name_ru
 * @property bool $is_published
 * @property string|null $description_en
 * @property string|null $description_kk
 * @property string|null $description_ru
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereDescriptionKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereNameKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    use HasFactory;

    protected $guarded = [];
}
