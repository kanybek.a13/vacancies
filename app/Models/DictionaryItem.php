<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DictionaryItem
 *
 * @property int $id
 * @property int $dictionary_id
 * @property string $name_en
 * @property string $name_kk
 * @property string $name_ru
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Dictionary $dictionary
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem whereDictionaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem whereNameKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DictionaryItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DictionaryItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function dictionary()
    {
        return $this->belongsTo(Dictionary::class);
    }
}
