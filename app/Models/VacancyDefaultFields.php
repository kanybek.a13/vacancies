<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VacancyDefaultFields
 *
 * @property int $id
 * @property int $source_id
 * @property string $name_en
 * @property string $name_kk
 * @property string $name_ru
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Dictionary $dictionary
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields query()
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereNameKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VacancyDefaultFields whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VacancyDefaultFields extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'vacancy_default_fields';

    public function dictionary()
    {
        return $this->belongsTo(Dictionary::class, 'source_id');
    }
}
