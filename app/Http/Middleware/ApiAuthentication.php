<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApiAuthentication
{
    const API_KEY_HEADER = 'x-api-key';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !$this->checkToken( $request ) ) {
            return response()->json( [ 'error' => 'Unauthorized' ], 403 );
        }

        return $next( $request );
    }

    public function checkToken( $request ) {
        $header = $request->header('Authorization', '');

        if (Str::startsWith($header, 'Bearer ')) {
            $api_token = Str::substr($header, 7);

            $user = User::where( 'api_token', $api_token )->first();

            return $user;
        }
    }
}
