<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Contracts\Role;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        Role::create('admin');
        return view('home');
    }

    public function permission()
    {
        $edit_permission = Permission::findByName('edit permission');
        $view_permission = Permission::findByName('view permission');

        $admin_role = \Spatie\Permission\Models\Role::findByName('admin');

        $admin_role->givePermissionTo('edit permission');
        $admin_role->givePermissionTo('view permission');

        dd($admin_role->permissions());
        return view('home');
    }
}
