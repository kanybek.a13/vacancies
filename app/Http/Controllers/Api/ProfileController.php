<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\MatchOldPassword;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    use ApiResponser;

    public function view(Request $request)
    {
        return $this->success( $request->user(), 'Profile data retrieved successfully.');
    }

    public function updateData(Request $request)
    {
        $data = $request -> validate([
            'name' => 'required|string|max:255|sometimes',
            'email' => 'required|email|sometimes',
        ]);

        $request->user()->update($data);

        return $this->success( $request->user(), 'Data updated successfully.');
    }

    public function updatePassword(Request $request)
    {
        $data = $request -> validate([
            'current_password' => ['required', new MatchOldPassword],
            'password' => 'required|string|min:8|max:255|confirmed',
        ]);

        auth()->user()->update(['password'=> Hash::make($data['password'])]);

        return $this->success('', 'Password updated successfully.');
    }
}
