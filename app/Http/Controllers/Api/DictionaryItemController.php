<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DictionaryItemRequest;
use App\Models\Dictionary;
use App\Models\DictionaryItem;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class DictionaryItemController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(Dictionary $dictionary){
        return $this->success( DictionaryItem::where('dictionary_id', $dictionary->id)
            ->paginate(12), "Dictionary Items retrieved successfully");
    }

    public function show( Dictionary $dictionary, DictionaryItem $item){
        if ($item = $dictionary->items()->find($item->id)){
            return $this->success( $item, "DictionaryItem retrieved successfully");
        }

        return $this->error( "Not found", 404);
    }

    public function store(DictionaryItemRequest $request, Dictionary $dictionary)
    {
        $data = $request->validated();

        if ($dictionaryItem = $dictionary->items()->create($data)) {
            return $this->success( $dictionaryItem, "Dictionary Item created successfully",201);
        }
    }

    public function update(Dictionary $dictionary, DictionaryItem $item, Request $request)
    {
        $data = $request -> validate([
            'name_en' => 'required|string|max:255|sometimes',
            'name_kk' => 'required|string|max:255|sometimes',
            'name_ru' => 'required|string|max:255|sometimes',
        ]);

        if ($item = $dictionary->items()->find($item->id)){
            $item->update($data);

            return $this->success( $item, "Dictionary Item updated successfully", 201);
        }

        return $this->error( "Not found", 404);
    }

    public function destroy(Dictionary $dictionary, DictionaryItem $item)
    {
        if ($item = $dictionary->items()->find($item->id)){
            if ($item->delete()){
                return $this->success('', "Dictionary Item deleted successfully");
            }
        }

        return $this->error( "Not found", 404);
    }
}
