<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DictionaryRequest;
use App\Models\Dictionary;
use App\Models\DictionaryItem;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(){
        return $this->success( Dictionary::paginate(12), "Dictionaries retrieved successfully",206);
    }

    public function show(Dictionary $dictionary){
        return $this->success( $dictionary, "Dictionary retrieved successfully");
    }

    public function store(DictionaryRequest $request)
    {
        $dictionary = Dictionary::create($request->validated());

        return $this->success( $dictionary, "Dictionary created successfully",201);
    }

    public function update(Dictionary $dictionary, Request $request)
    {
        $data = $request -> validate([
            'name_en' => 'required|string|max:255|sometimes',
            'name_kk' => 'required|string|max:255|sometimes',
            'name_ru' => 'required|string|max:255|sometimes',
        ]);

        $dictionary->update($data);

        return $this->success( $dictionary, "Dictionary updated successfully", 201);
    }

    public function destroy(Dictionary $dictionary)
    {
        if ($dictionary->delete()){
            return $this->success('', "Dictionary deleted successfully");
        }

        return $this->error( "Not found", 404);
    }

    public function items(Dictionary $dictionary){
        return $this->success([
            "items" => DictionaryItem::where('dictionary_id', $dictionary->id)
                ->paginate(12)
        ], "Items of dictionary retrieved successfully");
    }

    public function departments(Dictionary $dictionary)
    {
        return $this->success('','');
    }

    public function linkTargetTypes(Dictionary $dictionary)
    {
        return $this->success('','');
    }

    public function vacancyFieldTypes(Dictionary $dictionary)
    {
        return $this->success('','');
    }

    public function dictionaries(Dictionary $dictionary)
    {
        return $this->success('','');
    }
}
