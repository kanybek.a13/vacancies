<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VacancyDefaultFieldsRequest;
use App\Models\VacancyDefaultFields;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class VacancyDefaultFieldsController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(){
        return $this->success( VacancyDefaultFields::paginate(12), "Vacancy default fields retrieved successfully",206);
    }

    public function show(VacancyDefaultFields $vacancyDefaultField){
        return $this->success( $vacancyDefaultField, "Vacancy default field retrieved successfully");
    }

    public function store(VacancyDefaultFieldsRequest $request)
    {
        $data = $request->validated();

        if ($data['type'] == 3){
            $data['source_id'] = null;
        }

        $fields = VacancyDefaultFields::create($data);

        return $this->success( $fields, "Vacancy default field created successfully",201);
    }

    public function update(VacancyDefaultFields $vacancyDefaultField, Request $request)
    {
        $data = $request -> validate([
            'name_en' => 'required|string|max:255|sometimes',
            'name_kk' => 'required|string|max:255|sometimes',
            'name_ru' => 'required|string|max:255|sometimes',
            'type' => 'required|int|between:0,3',
            'source_id' => 'required_unless:type,3int|exists:dictionaries,id',
        ]);

        $vacancyDefaultField->update($data);

        return $this->success( $vacancyDefaultField, "Vacancy default field updated successfully", 201);
    }

    public function destroy(VacancyDefaultFields $vacancyDefaultField)
    {
        if ($vacancyDefaultField->delete()){
            return $this->success('', "Vacancy default field deleted successfully");
        }

        return $this->error( "Not found", 404);
    }
}
