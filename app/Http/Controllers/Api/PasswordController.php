<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\PasswordResetMail;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class PasswordController extends Controller
{
    use ApiResponser;

    public function generateResetCode(Request $request){
        $email = $request->validate([
            'email' => 'required|email'
        ]);

        if($user = User::where('email', $email)->first()){
            $code = Str::random(4);

            Mail::to($user->email)->send(new PasswordResetMail($user, $code));

            $user->update(['password_reset_code' => $code]);

            return $this->success('','Code sended to email.');
        }

        return $this->error('Email not found',404);
    }

    public function reset(Request $request)
    {
        $data = $request->validate([
            'code' => 'required|string'
        ]);

        $user = User::where('password_reset_code', $data['code'])->first();

        if($user){
            $password = Str::random(8);

            $user->update([
                'password' => Hash::make($password),
                'password_reset_code' => null
            ]);

            Mail::to($user->email)->send(new \App\Mail\PasswordNewMail($user, $password));

            return $this->success('', 'New password sended to your email.');
        }

        return $this->error('Not found',404);
    }
}
