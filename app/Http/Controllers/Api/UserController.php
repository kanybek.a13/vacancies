<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\Role;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index()
    {
        return $this->success( User::paginate(12), "Users retrieved successfully", 206);
    }

    public function show(User $user)
    {
        return $this->success( $user, "User retrieved successfully");
    }

    public function store(RegisterRequest $request)
    {
        $data = $request->validated();
        $password = Str::random(8);

        $data['password'] = Hash::make($password);

        $user = User::create($data);
        $user->assignRole(Role::findByName('employee'));

        Mail::to($user->email)->send(new \App\Mail\WelcomeMail($user, $password));

        return $this->success( $user, "Created successfully", 201);
    }

    public function update(User $user, Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255|sometimes',
            'iin' => 'required|integer|digits:12|sometimes|' . Rule::unique('users')->ignore($user->id),
            'email' => 'required|string|email|max:255|sometimes|' . Rule::unique('users')->ignore($user->id),
        ]);

        $password = Str::random(8);

        $data['password'] = Hash::make($password);

        $user->update($data);

        Mail::to($user->email)->send(new \App\Mail\WelcomeMail($user, $password));

        return $this->success( $user, "Updates successfully", 201);
    }

    public function destroy(User $user)
    {
        if ($user->delete()) {
            return $this->success('', "Deleted successfully");
        }

        return $this->error('Not found', 404);
    }
}
