<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LinkRequest;
use App\Models\Link;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class LinkController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(){
        return $this->success( Link::paginate(12), "Links retrieved successfully");
    }

    public function show(Link $link){
        return $this->success( $link,"Link retrieved successfully");
    }

    public function store(LinkRequest $request)
    {
        $link = Link::create($request->validated());

        return $this->success( $link, "Created successfully", 201);
    }

    public function update(Link $link, Request $request)
    {
        $data = $request -> validate([
            'name_en' => 'required|string|max:255',
            'name_kk' => 'required|string|max:255',
            'name_ru' => 'required|string|max:255',
            'link_en' => 'required|string|max:255',
            'link_kk' => 'required|string|max:255',
            'link_ru' => 'required|string|max:255',
            'sort' => 'required|integer',
            'target' => Rule::in(['_self', '_blank']),
        ]);

        $link->update($data);

        return $this->success( $link, "Updates successfully", 201);
    }

    public function destroy(Link $link)
    {
        if ($link->delete()){
            return $this->success('', 'Deleted successfully');
        }

        return $this->error('Not found', 404);
    }
}
