<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\Models\Page;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(){
        return $this->success( Page::paginate(12), "Pages retrieved successfully");
    }

    public function show(Page $page){
        return $this->success( $page, "Page retrieved successfully");
    }

    public function store(PageRequest $request)
    {
        $data = $request->validated();

        $data['description_en'] = json_encode($data['description_en']);
        $data['description_kk'] = json_encode($data['description_kk']);
        $data['description_ru'] = json_encode($data['description_ru']);

        $page = Page::create($data);

        return $this->success( $page, "Created successfully", 201);
    }

    public function update(Request $request, Page $page)
    {
        $data = $request -> validate([
            'alias' => 'required|string|max:255|' . Rule::unique('pages')->ignore($page->id),
            'name_en' => 'string|max:255',
            'name_kk' => 'string|max:255',
            'name_ru' => 'string|max:255',
            'description_en' => 'required',
            'description_ru' => 'required',
            'description_kk' => 'required',
        ]);

        $data['description_en'] = json_encode($data['description_en']);
        $data['description_kk'] = json_encode($data['description_kk']);
        $data['description_ru'] = json_encode($data['description_ru']);

        $page->update($data);

        return $this->success( $page, "Updates successfully", 201);
    }

    public function destroy(Page $page)
    {
        if ($page->alias != 'home') {
            if ($page->delete()) {
                return $this->success('', 'Deleted successfully');
            }
        }

        return $this->error('Home page could not be deleted!', 403);
    }
}
