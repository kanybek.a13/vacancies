<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VacancyTemplateRequest;
use App\Models\VacancyTemplate;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class VacancyTemplateController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(){
        return $this->success( VacancyTemplate::paginate(12), "Vacancy Templates retrieved successfully",206);
    }

    public function show(VacancyTemplate $vacancyTemplate){
        return $this->success( $vacancyTemplate, "Vacancy Template retrieved successfully");
    }

    public function store(VacancyTemplateRequest $request)
    {
        $vacancyTemplate = VacancyTemplate::create($request->validated());

        return $this->success( $vacancyTemplate, "Vacancy Template created successfully",201);
    }

    public function update(VacancyTemplate $vacancyTemplate, Request $request)
    {
        $data = $request -> validate([
            'name_en' => 'required|string|max:255|sometimes',
            'name_kk' => 'required|string|max:255|sometimes',
            'name_ru' => 'required|string|max:255|sometimes',
        ]);

        $vacancyTemplate->update($data);

        return $this->success( $vacancyTemplate, "Vacancy Template updated successfully", 201);
    }

    public function destroy(VacancyTemplate $vacancyTemplate)
    {
        if ($vacancyTemplate->delete()){
            return $this->success('', "Vacancy Template deleted successfully");
        }

        return $this->error( "Not found", 404);
    }
}
