<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentRequest;
use App\Models\Appeal;
use App\Models\Department;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(){
        return $this->success( Department::paginate(12),"Departments retrieved successfully",206);
    }

    public function show(Department $department){
        return $this->success( $department, "Department retrieved successfully");
    }

    public function store(DepartmentRequest $request)
    {
        if (auth()->user()->hasPermissionTo('store_update', 'api')) {
            return $this->success( Department::create($request->validated()), "Department created successfully",201);
        }

        return $this->error('You can not create a new department', 403);
    }

    public function update(Department $department, Request $request)
    {
        if (auth()->user()->hasPermissionTo('store_update', 'api')) {
            $data = $request -> validate([
                'name_en' => 'required|string|max:255|sometimes',
                'name_kk' => 'required|string|max:255|sometimes',
                'name_ru' => 'required|string|max:255|sometimes',
            ]);

            $department->update($data);

            return $this->success( $department, "Department updated successfully", 201);
        }

        return $this->error( 'You can not update departments', 403);
    }

    public function destroy(Department $department)
    {
        if (auth()->user()->hasPermissionTo('edit_department', 'api')) {
            if ($department->delete()){
                return $this->success('', "Department deleted successfully");
            }

            return $this->error( "Not found", 404);
        }

        return $this->error('You can not delete departments', 403);
    }

    public function users(Department $department){
        return $this->success( User::where('department_id', $department->id)
                ->paginate(12), "Users of department retrieved successfully");
    }
}
