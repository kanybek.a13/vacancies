<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VacancyTemplateFieldRequest;
use App\Models\VacancyTemplate;
use App\Models\VacancyTemplateField;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class VacancyTemplateFieldController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware(['permission:store_update'])->only(['store', 'store']);
        $this->middleware(['permission:delete'])->only('destroy');
    }

    public function index(VacancyTemplate $vacancyTemplate){
        return $this->success( VacancyTemplateField::where('vacancy_template_id', $vacancyTemplate->id)
            ->paginate(12), "Vacancy Template Fields retrieved successfully");
    }

    public function show( VacancyTemplate $vacancyTemplate, VacancyTemplateField $field){
        if ($field = $vacancyTemplate->fields()->find($field->id)){
            return $this->success( $field, "Vacancy Template Field retrieved successfully");
        }

        return $this->error( "Not found", 404);
    }

    public function store(VacancyTemplateFieldRequest $request, VacancyTemplate $vacancyTemplate)
    {
        $data = $request->validated();

        if ($field = $vacancyTemplate->fields()->create($data)) {
            return $this->success( $field, "Vacancy Template Field created successfully",201);
        }
    }

    public function update(Request $request, VacancyTemplate $vacancyTemplate, VacancyTemplateField $field)
    {
        $data = $request -> validate([
            'name_en' => 'required|string|max:255|sometimes',
            'name_kk' => 'required|string|max:255|sometimes',
            'name_ru' => 'required|string|max:255|sometimes',
        ]);

        if ($field = $vacancyTemplate->fields()->find($field->id)){
            $field->update($data);

            return $this->success( $field, "Vacancy Template Field updated successfully", 201);
        }

        return $this->error( "Not found", 404);
    }

    public function destroy(VacancyTemplate $vacancyTemplate, VacancyTemplateField $field)
    {
        if ($field = $vacancyTemplate->fields()->find($field->id)){
            if ($field->delete()){
                return $this->success('', "Vacancy Template Field deleted successfully");
            }
        }

        return $this->error( "Not found", 404);
    }
}
