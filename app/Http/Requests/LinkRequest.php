<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => ['required', 'string', 'max:255'],
            'name_ru' => ['required', 'string', 'max:255'],
            'name_kk' => ['required', 'string', 'max:255'],
            'link_en' => ['required', 'string', 'max:255'],
            'link_ru' => ['required', 'string', 'max:255'],
            'link_kk' => ['required', 'string', 'max:255'],
            'sort' => ['required', 'integer'],
            'target' => Rule::in(['_self', '_blank']),
        ];
    }
}
