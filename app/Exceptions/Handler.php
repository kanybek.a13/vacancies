<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponser;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $ex
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $ex)
    {
        if($ex instanceof UnauthorizedException) {
            return $this->error("Unauthorized action",403);
        } else if($ex instanceof NotFoundHttpException or $ex instanceof ModelNotFoundException) {
            return $this->error("Not found", 404);
        } else if ($ex instanceof AuthenticationException){
            return $this->error("Not authorized", 401);
        } else if ($ex instanceof PermissionDoesNotExist){
            return $this->error("Permission not found authorized", 404);
        } else if ($ex instanceof ValidationException) {
            return $this->error("Validation Error", 404, [
                'type' => 'form',
                'description' => 'Bad request.',
                'detail' => $ex->validator->errors()
            ]);
        }

        return parent::render($request, $ex);
    }
}
