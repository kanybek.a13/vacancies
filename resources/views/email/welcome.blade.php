@component('mail::message')
# Hello {{$user->fullname}}!

Welcome to our application!

Your password is: {{$password}}

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
