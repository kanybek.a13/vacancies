@component('mail::message')
# Hello {{$user->fullname}}!

Your new password is: {{$password}}

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
