@component('mail::message')
# Hello {{$user->fullname}}!

Welcome to our application!

Your code to reset password is: {{$code}}

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
